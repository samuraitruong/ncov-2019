echo $1
curl -o -vvv -X POST \
  https://api.travis-ci.com/repo/samuraitruong%2Fncov-2019/requests \
  -H 'Accept: application/json' \
  -H 'Authorization: token ' $1 \
  -H 'Content-Length: 91' \
  -H 'Content-Type: application/json' \
  -H 'Travis-API-Version: 3' \
  -d '{
    "request": {
    	"message": "Schedule job build",
        "branch": "master"
    }
}'